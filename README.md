# Living Without Google

Living Without Google is a simple, quick and straightforward advice to use computer and internet freely and practically without using Google Search, Gmail, Chrome, YouTube, Drive, Docs, Forms, Hangouts and Meet. Based on real life use in [software freedom](https://www.gnu.org/philosophy/free-sw.en.html) by Malsasa. 

## 1. Without Google Search

Use Startpage Search https://startpage.com. 

## 2. Without Gmail 

Use [Mailo](https://mailo.com) or [Disroot Mail](https://disroot.org). You will get free email account e.g. your_name@mailo.com or your_name@disroot.org with IMAP access.

## 3. Without Chrome 

Use [Firefox Browser](https://mozilla.org). 

## 4. Without YouTube 

Use [Invidious](https://invidio.us) on computer and [Newpipe](https://f-droid.org/en/packages/org.schabi.newpipe/) on mobile (install from F-Droid), while getting started to know [PeerTube](https://joinpeertube.org). 

## 5. Without Blogspot

Use [WordPress.com](https://wordpress.com).

## 6. Without AdSense

Use [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin) to remove advertisements on videos and web pages. Consider using your social skills and speeches to earn money from advertising.

## 7. Without Google Forms 

Use [CryptPad](https://cryptpad.fr). 

## 8. Without Google Drive 

Use CryptPad, or [Nextcloud](https://nextcloud.com/signup), or [Internet Archive](https://archive.org). CryptPad for small files, Nextcloud for big files, and Internet Archive to share publicly without size limits.

## 9. Without Google Docs 

Use CryptPad. 

## 10. Without Google Hangouts

Use [Telegram](https://telegram.org). Other choices, [Element](https://element.io) or [XMPP](https://movim.eu).

## 11. Without Google Meet 

Use [Jitsi Meet](https://meet.jit.si).

## 12. Without Google DNS

Use DNS.Watch's (84.200.69.80 84.200.70.40) IP addresses. Don't forget to enable DNS Over HTTPS (Nextdns) on Firefox.

## 13. Without Google reCaptcha

Use [CaptCheck](https://captcheck.netsyms.com/). Otherwise, see [SecureImages](https://www.phpcaptcha.org/) or [VisualCaptcha](https://visualcaptcha.net).

## 14. Without Google Play Store 

Use [F-Droid](https://f-droid.org). Download and install the program on your Android phone.

## 15.  Without Google Plus

Use [Mastodon Social Media](https://joinmastodon.org). 

## 16. Without Android 

Use no phone, otherwise use [LineageOS](https://lineageos.org/).  

